# Documentação (Agendamentos - Conexa Saúde)
<hr/>
<img src="src/assets/logos/default.svg" width="auto" height="50px"/>

# Back-end
Não continha as rotas:
- Buscar Consultas => GET http://localhost:3333/consultations?_expand=patient
- Nova Consulta => POST http://localhost:3333/consultations

## Added

### server
```javascript
server.post('/consultations', registerSchedules);
server.get('/consultations', dataSchedules);
```

### Handlers/schedules
```javascript
/**
 * @author Hector Rodrigues da Silva
 */
const path = require('path')
const db = require(path.join(__dirname, '../db.json'));

let localDatabase = Object.assign({}, db);

/**
 *
 * @param req
 * @param response
 * @returns {any}
 */
const registerSchedules = (req, response) => {
    const { patient_id, date } = req.body;
    localDatabase.consultations.push({
        date: date,
        patientId: Number(patient_id),
        id: localDatabase.consultations.length + 1
    });
    return response.status(200).json({ message: 'Successfully registered' });
}

/**
 *
 * @param _req
 * @param response
 * @returns {any}
 */
const dataSchedules = (_req, response) => {
    const payload = Object.assign({}, localDatabase);
    payload.consultations.forEach(consult => {
        payload.patients.forEach(patient => {
            if(consult.patientId === patient.id) {
                const currentConsults = patient.consults?.slice() ?? [];
                delete consult.patientId;
                patient.consults = [...currentConsults, consult];
            }
        });
    });
    delete payload.consultations;
    return response.status(200).json(payload);
    // ** RESULT **
        // [
        //     {
        //         "id": 1,
        //         "first_name": "Frodo",
        //         "last_name": "Baggins",
        //         "email": "frodo.baggins@mail.com",
        //         "consults": [
        //             {
        //                 "id": 1,
        //                 "date": "Fri Feb 05 2021 10:20:00 GMT-0300 (Brasilia Standard Time)"
        //             }
        //         ]
        //     },
        //     {
        //         "id": 2,
        //         "first_name": "Samwise",
        //         "last_name": "Gamgee",
        //         "email": "samwise.gamgee@mail.com",
        //         "consults": [
        //             {
        //                 "id": 3,
        //                 "date": "Thu Feb 11 2021 10:00:00 GMT-0300 (Brasilia Standard Time)"
        //             }
        //         ]
        //     },
        //     {
        //         "id": 3,
        //         "first_name": "Saruman",
        //         "last_name": "The White",
        //         "email": "saruman.thewhite@mail.com",
        //         "consults": [
        //             {
        //                 "id": 2,
        //                 "date": "Thu Feb 11 2021 09:00:00 GMT-0300 (Brasilia Standard Time)"
        //             },
        //             {
        //                 "id": 4,
        //                 "date": "Thu Feb 11 2021 13:00:00 GMT-0300 (Brasilia Standard Time)"
        //             }
        //         ]
        //     }
        // ]
}

module.exports = {dataSchedules, registerSchedules};
```

# Front-end

<a href="https://developer.mozilla.org/pt-BR/docs/Web/Guide/AJAX" rel="nofollow">
<img src="https://camo.githubusercontent.com/f4863e028d21ce99fa704116b3afcaab7ac5ea6a0c7bd3f0ce10b80e6a85f17a/68747470733a2f2f63646e2e646973636f72646170702e636f6d2f6174746163686d656e74732f3739343031353938353435303335323636312f3935383338393736353732363637303937382f3130323470782d414a41585f6c6f676f5f62795f67656e676e732e706e67" alt="AJAX" width="auto" height="35" style="max-width: 100%;">
</a>&nbsp;
<a href="https://reactjs.org/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original-wordmark.svg" alt="react" width="50" height="auto"/>
</a>&nbsp;
<a href="https://reactrouter.com/">
<img src="https://cdn.discordapp.com/attachments/794015985450352661/984474400239812608/unknown.png" alt="React Router" width="auto" height="40px" style="max-width: 100%"/>
</a>&nbsp;
<a href="https://www.typescriptlang.org/" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/typescript/typescript-original.svg" alt="typescript" width="45" height="auto"/>
</a>&nbsp;
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer">
<img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="45" height="auto"/>
</a>&nbsp;
<a href="https://gulpjs.com/" rel="nofollow">
<img src="https://camo.githubusercontent.com/c9240a4555d87d597dc9b59196fec4b751ef62505882ec3454a11135ed967469/68747470733a2f2f63646e2e646973636f72646170702e636f6d2f6174746163686d656e74732f3739343031353938353435303335323636312f3935383336303631313439373035343233382f67756c702d32782e706e67" alt="gulpjs" width="auto" height="50" style="max-width: 100%;">
</a>&nbsp;
<a href="https://git-scm.com/" rel="nofollow">
<img src="https://camo.githubusercontent.com/fbfcb9e3dc648adc93bef37c718db16c52f617ad055a26de6dc3c21865c3321d/68747470733a2f2f7777772e766563746f726c6f676f2e7a6f6e652f6c6f676f732f6769742d73636d2f6769742d73636d2d69636f6e2e737667" alt="git" width="auto" height="45" style="max-width: 100%;">
</a>&nbsp;
<a href="https://getbootstrap.com" rel="nofollow"> 
<img src="https://camo.githubusercontent.com/999637a373f9d66fcb7eefc54dd1a25eb920f265c34b5ffd53d2ab906ccdb01a/68747470733a2f2f63646e2e646973636f72646170702e636f6d2f6174746163686d656e74732f3739343031353938353435303335323636312f3935383337313837333637373235383736322f3132303070782d426f6f7473747261705f6c6f676f2e706e67" alt="Bootstrap" width="auto" height="45" style="max-width: 100%;">
</a>&nbsp;
<a href="https://styled-components.com/" rel="nofollow">
<img src="https://camo.githubusercontent.com/baed8d0e768e47c1780066227cd5cbc010f2451d87a2bfceb2d24687a66435d9/68747470733a2f2f63646e2e646973636f72646170702e636f6d2f6174746163686d656e74732f3739343031353938353435303335323636312f3935383133353739313133363136353934382f32303635383832352e706e67" alt="styled-components" width="auto" height="45" style="max-width: 100%;">
</a>&nbsp;
<a href="https://fonts.google.com/icons">
<img src="https://cdn.discordapp.com/attachments/794015985450352661/984473465631440986/unknown.png" alt="Google Material Icons" width="auto" height="50px" style="max-width: 100%;"/>
</a>&nbsp;
<a href="https://momentjs.com/">
<img src="https://cdn.discordapp.com/attachments/794015985450352661/984473758322548776/unknown.png" alt="Moment.JS" width="auto" height="50px" style="max-width: 100%"/>
</a>&nbsp;
<a href="https://react-window.vercel.app/#/examples/list/fixed-size">
<img src="https://cdn.discordapp.com/attachments/794015985450352661/984475144519036968/unknown.png" alt="React Window" width="auto" height="50px" style="max-width: 100%"/>
</a>

## Core

Alert
```javascript
type = {
    "primary"
    "secondary"
    "success"
    "danger"
    "warning"
    "info" // DEFAULT
    "light"
    "dark"
}

<Alert
   {...}
   show={true} 
   title="title" 
   type="danger" 
   className=""
>
    ...
</Alert>
```

<hr/>

Box
```javascript
<Box className="" {...}>
    ...
</Box>
```

<hr/>

Button
```javascript
<Button
    {...}
    size="lg"
    className=""
    icon="login" // material icons
    outline={false}
    secondary={true}
    onClick={() => {}}
>
    ...
</Button>
```

<hr/>

Card
```javascript
<Card className="" {...}>
    ...
</Card>
```

<hr/>

Fallback
```javascript
<Suspense fallback={<LoaderFallback/>}>
    ...
</Suspense>
```

<hr/>

Grid (Col)
```javascript
cols = "sm md lg xl xxl";
    
<Col
    {...}
    cols="12 12 12 5 5 classNames"
>
    ...
</Col>
```

<hr/>

Grid (Row)
```javascript
<Row className="" {...}>
    ...
</Row>
```

<hr/>

Icon (Material Icons)
```javascript
 type = {
     "outlined" // DEFAULT
     "filled"
     "rounded"
     "sharp"
     "two-tone"
}

 <MaterialIcon
    size={15} // PIXELS
    style={{}}
    icon="login" // REQUIRED
    color="#fff" 
    hover={false}
    pointer={false}
/>
```

<hr/>

Input Default
```javascript
<InputDefault
    {...}
    error={true}
/>
```

<hr/>

Input Label
```javascript
<InputLabel
    {...}
    label=""
    error={true}
/>
```

<hr/>

Input Password
```javascript
<InputPassword
    {...}
    label=""
    error={true}
/>
```

<hr/>

Input Password Label
```javascript
<InputPasswordLabel
    {...}
    label=""
    error={true}
/>
```

Loaders
```javascript
<LoadingDefault
    {...}
    size={30} // PIXELS
    style={{}}
    color="#fff"
/>
```

Modal
```javascript
const [show, setShow] = useState(true);

<Modal show={show} handleClose={() => setShow(false)}>
    <ModalHeader title=""/>
    <ModalBody>
       ...
    </ModalBody>
    <ModalFooter>
        ...
    </ModalFooter>
</Modal>
```

Render
```javascript
<Render condition={true}>
    ...
</Render>
```

`npm i or yarn`

`npm start or yarn start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

#### Build
* Command `npm run build or yarn build`

#### Optimization
* Command `npm i -g gulp`
* Command `gulp build`

**@authors: <a href="https://github.com/HectorFront">Hector Rodrigues da Silva</a>**
